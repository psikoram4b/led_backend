/**
 * Partner.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    name: {
      type: "string",
      required: true
    },
    contact: {
      type: "string",
      required: true
    },
    address: {
      type: "string"
    },
    email: {
      type: "string",
      required: true
    },
    password: { //jakoś inaczej?
      type: "string",
      required: true
    },
    NIP: {
      type: "string",
      required: true
    },
    category: {
      model: "partnerCategory"
    },
    projectNumberPrefix: {
      type: 'string'
    }

  }
};
