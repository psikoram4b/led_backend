/**
 * PartnerController
 *
 * @description :: Server-side logic for managing partners
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  login: function(req, res, next) {
    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }
    if (!req.param('email')) {
      return res.badRequest(
        'no email in params.'
      );
    }
    if (!req.param('password')) {
      return res.badRequest(
        'no password in params.'
      );
    }

    Partner.findOne({
      email: req.param('email'),
      password: req.param('password')
    }).exec(function(err, partner) {
      if (err || !partner)
        return res.badRequest('invalid email or password');
      else
        return res.ok(partner)
    });

  },


  new: function(req, res, next) {
    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }
    if (!req.param('name')) {
      return res.badRequest(
        'no name in params.'
      );
    }
    if (!req.param('contact')) {
      return res.badRequest(
        'no contact in params.'
      );
    }
    if (!req.param('email')) {
      return res.badRequest(
        'no email in params.'
      );
    }
    if (!req.param('password')) {
      return res.badRequest(
        'no password in params.'
      );
    }
    if (!req.param('NIP')) {
      return res.badRequest(
        'no name in params.'
      );
    }

    Partner.count({
      NIP: req.param('NIP')
    }, function(err, existing) {
      if (err)
        return res.status(500).send(err);
      else if (existing > 0)
        return res.badRequest('This partner is already registered');
      else {
        Partner.create({
          name: req.param('name'),
          contact: req.param('contact'),
          address: req.param('address'),
          email: req.param('email'),
          password: req.param('password'),
          NIP: req.param('NIP')
        }, function(err2, rows) {
          if (err2)
            return res.staus(500).send(err2);
          else return res.ok(rows);
        })
      }
    });
  },

  list: function(req, res, next) {
    Partner.find({}, function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);
    })
  },

  update: function(req, res, next) {
    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }
    if (!req.param('id')) {
      return res.badRequest(
        'no id in params.'
      );
    }
    if (!req.param('changes')) {
      return res.badRequest(
        'no changes in params.'
      );
    }

    Partner.update({
      id: req.param('id')
    }).set(req.param('changes')).exec(function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);
    })

  },

  newCategory: function(req, res, next) {
    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }
    if (!req.param('name')) {
      return res.badRequest(
        'no name in params.'
      );
    }
    if (req.param('discount') == 'undefined') {
      return res.badRequest(
        'no discount in params.'
      );
    }
    PartnerCategory.create({
      name: req.param('name'),
      discount: parseInt(req.param('discount'))
    }, function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);

    })

  },

  assignCategory: function(req, res, next) {
    Partner.update({
      id: req.param('partnerId')
    }).set({
      category: req.param('categoryId')
    }).exec(function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);
    })
  },

  listCategories: function(req, res, next) {
    PartnerCategory.find({}, function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);
    })
  }

}
