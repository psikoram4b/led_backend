/**
 * ProjectController
 *
 * @description :: Server-side logic for managing projects
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  new: function(req, res, next) {
    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }
    if (!req.param('name')) {
      return res.badRequest(
        'no name in params.'
      );
    }
    if (!req.param('description')) {
      return res.badRequest(
        'no contact in params.'
      );
    }
    if (!req.param('partner')) {
      return res.badRequest(
        'no email in params.'
      );
    }
    if (!req.param('client')) {
      return res.badRequest(
        'no password in params.'
      );
    }

    var newProject = {
      name: req.param('name'),
      description: req.param('description'),
      value: req.param('value'),
      deliveryDate: req.param('value'),
      partner: req.param('partner'),
      client: req.param('client')
    };

    Partner.findOne({
      id: newProject.partner
    }).exec(function(err, partner) {

      var projectNumber = partner.projectNumberPrefix + sails.moment.format('YYMM');
      Project.count({
        projectNumber: {
          "startsWith": projectNumber
        }
      }, function(err, count) {
        if (err)
          return res.status(500).send(err);
        else {
          newProject.projectNumber = projectNumber + (count + 1);
          Project.create(newProject, function(err, row) {
            if (err)
              return res.status(500).send(err);
            else
              return res.ok(rows);
          })
        }

      })
    });
  },

  list: function(req, res, next) {
    Project.find({}, function(err, rows) {
      if (err)
        return res.status(500).send(err);
      else
        return res.ok(rows);
    })
  }
}
