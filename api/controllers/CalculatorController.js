/**
 * CalculatorController
 *
 * @description :: Server-side logic for managing calculators
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {





  /**
   * `CalculatorController.get()`
   */
  get: function(req, res) {

    if (req.method != "POST") {
      return res.badRequest(
        'not a POST request.'
      );
    }

    if (!req.param('wall') && !req.param('moduleLayout'))
      return res.badRequest(
        'Insufficient layout data. Please provide module layout or wall size'
      );
    // 
    // if (!req.param('module'))
    //   return res.badRequest(
    //     'Insufficient module data.'
    //   );

    if (!req.param('pixelPitch'))
      return res.badRequest(
        'Insufficient module data.'
      );

    // var parameters = {
    //   wall: req.param('wall'),
    //   moduleLayout: req.param('moduleLayout'),
    //   module: req.param('module')
    // }

    var parameters = req.body;

    var result = calculateLayout(parameters);

    return res.json(result);
  }
};

var moduleTypes = {
  "P4": {
    width: 256,
    height: 256,
    resolutionX: 64,
    resolutionY: 64,
    avgPower: 20,
    maxPower: 40,
    use2x4ReceiverLayout: false
  },
  "P3": {
    width: 320,
    height: 160,
    resolutionX: 96,
    resolutionY: 48,
    avgPower: 16,
    maxPower: 32,
    use2x4ReceiverLayout: true
  },
  "P2": {
    width: 320,
    height: 160,
    resolutionX: 128,
    resolutionY: 64,
    avgPower: 19,
    maxPower: 38,
    use2x4ReceiverLayout: true
  }
};

function calculateLayout(parameters) {

  var result = {};
  //liczenie ilości modułów

  var module = moduleTypes[parameters.pixelPitch];

  switch (parameters.sizeType) {
    case "numberOfColumnsAndRows":
      result.noOfModulesX = parameters.moduleLayout.x;
      result.noOfModulesY = parameters.moduleLayout.y;
      break;
    case "wall":
    case "imageSize":
      result.noOfModulesX = Math.floor(parameters.wall.Width / module.Width);
      result.noOfModulesY = Math.floor(parameters.wall.Height / module.Height);
      break;
    case "resolution":
      result.noOfModulesX = Math.floor(parameters.resolution.Width / module.resolutionX);
      result.noOfModulesY = Math.floor(parameters.resolution.Height / module.resolutionY);
      break;
    default:

  }

  // if (!parameters.moduleLayout) {
  //   result.noOfModulesX = Math.floor(parameters.wall.Width / parameters.module.Width);
  //   result.noOfModulesY = Math.floor(parameters.wall.Height / parameters.module.Height);
  // } else {
  //   result.noOfModulesX = parameters.moduleLayout.x;
  //   result.noOfModulesY = parameters.moduleLayout.y;
  // }
  result.totalWidth = result.noOfModulesX * module.width;
  result.totalHeight = result.noOfModulesY * module.height;
  result.totalDepth = 700;
  result.area = result.totalWidth * result.totalHeight / 1000000;
  result.diagonal = Math.sqrt(Math.pow(result.totalWidth, 2) + Math.pow(result.totalHeight, 2));
  result.totalWeight = result.totalWidth * result.totalHeight * 0.00002;
  result.resolutionX = result.noOfModulesX * module.resolutionX;
  result.resolutionY = result.noOfModulesY * module.resolutionY;

  //wyliczenie kabinetów
  //poziom
  var cabinetsX = [];
  var totalCabsX = 0;
  while (totalCabsX < result.noOfModulesX - 3) {
    var cabsLeft = result.noOfModulesX - 3 - totalCabsX;
    if (cabsLeft == 4) {
      totalCabsX += 2;
      cabinetsX.unshift(2);
    } else if (cabsLeft == 2) {
      totalCabsX += 2;
      cabinetsX.push(2);
    } else {
      totalCabsX += 3;
      cabinetsX.push(3);
    }
  }
  //dodaje 1.5 na początku i na końcu
  cabinetsX.push(1.5);
  cabinetsX.unshift(1.5);

  //pion
  var cabinetsY = [];
  var totalCabsY = 0;
  if (result.noOfModulesY == 8) {
    cabinetsY = [4, 4];
    totalCabsY = 8;
  } else
    while (totalCabsY < result.noOfModulesY) {
      var cabsLeft = result.noOfModulesY - totalCabsY;

      if (cabsLeft == 3 || cabsLeft == 6) {
        totalCabsY += 3;
        cabinetsY.push(3);
      } else if (cabsLeft == 4 || cabsLeft == 7) {
        totalCabsY += 4;
        cabinetsY.push(4);
      } else {
        totalCabsY += 5;
        cabinetsY.push(5);
      }
    }
  cabinetsY.sort();
  result.cabinetsX = cabinetsX;
  result.cabinetsY = cabinetsY;

  //ilość zasilaczy

  result.noOfPowerSupplies = Math.ceil(result.noOfModulesX / 2) * Math.ceil(result.noOfModulesY / 2);


  //inicjalizacja layoutu

  var layout = {
    rows: new Array(result.noOfModulesY),
  };

  for (var y = 0; y < result.noOfModulesY; y++) {

    layout.rows[y] = {
      powerSupplyRail: false,
      cabinetBottom: false
    }
    layout.rows[y].modules = new Array(result.noOfModulesX);
    for (var x = 0; x < result.noOfModulesX; x++) {
      layout.rows[y].modules[x] = {
        cabinetEnd: false
      }
    }

  }


  // var layout = new Array(result.noOfModulesX);
  // // console.log("Layout length", layout.length);
  // for (var i = 0; i < layout.length; i++) {
  //   layout[i] = new Array(result.noOfModulesY);
  //   for (var j = 0; j < layout[i].length; j++)
  //     layout[i][j] = {
  //       x: i,
  //       y: j,
  //       cabinetEnd: false,
  //       powerSupplyRail: false
  //     };
  //
  // }
  //rozmieszczenie pionowych łączeń cabinetów (tam się nie da zasilaczy)
  var moduleIterator = 0;
  for (var i = 0; i < cabinetsX.length - 1; i++) {
    moduleIterator += cabinetsX[i];
    // console.log("Current module iterator", moduleIterator);
    var x = Math.floor(moduleIterator)
    for (var y = 0; y < result.noOfModulesY; y++) {
      // console.log("Setting " + column + "," + j + " to true");
      layout.rows[y].modules[x].cabinetEnd = true;

    }
  }

  //rozmieszczenie szyn do montażu zasilaczy
  var y = 0;
  for (var i = 0; i < cabinetsY.length; i++) {
    var railPositions = []
    if (cabinetsY[i] == 3)
      railPositions = [0, 2];
    if (cabinetsY[i] == 4)
      railPositions = [1, 2];
    if (cabinetsY[i] == 5)
      railPositions = [0, 2, 4];

    for (var j = 0; j < railPositions.length; j++) {
      // console.log("Rail at row", (y + railPositions[j]));
      // for (var x = 0; x < result.noOfModulesX; x++) {

      layout.rows[y + railPositions[j]].powerSupplyRail = true;
      // }
    }

    y += cabinetsY[i];
    layout.rows[y - 1].cabinetBottom = true;
  }


  //rozmieszczenie zasilaczy, lecimy kwadratami po 2x2
  var noOfSuppliesPlaced = 0;
  for (var y = 0; y < result.noOfModulesY; y += 2)
    for (var x = 0; x < result.noOfModulesX; x += 2) {
      var supplyPlaced = false;
      for (var dx = 0; dx < 2; dx++)
        for (var dy = 0; dy < 2; dy++) {
          if (x + dx < result.noOfModulesX && y + dy < result.noOfModulesY) {
            layout.rows[y + dy].modules[x + dx].supplyNumber = noOfSuppliesPlaced + 1;
            if (!supplyPlaced && layout.rows[y + dy].powerSupplyRail && !layout.rows[y + dy].modules[x + dx].cabinetEnd) {
              layout.rows[y + dy].modules[x + dx].supplyFacing = dx == 0 ? "right" : "left";
              supplyPlaced = true;
            } else layout.rows[y + dy].modules[x + dx].supplyFacing = "none";
          }
        }
      if (supplyPlaced)
        noOfSuppliesPlaced++;
      else
        console.log("SUM TING WONG!!!");
    }

  // console.log("Number of supplies placed", noOfSuppliesPlaced);
  var receiverLayoutX = result.cabinetsY.includes(5) ? 3 : 4;

  //przypisanie modułów do kart odbiorczych
  var currentReceiver = 0;
  var initialPaint = false;
  var currentCab = 0;
  for (var y = 0; y < result.noOfModulesY; y += result.cabinetsY[currentCab++]) {
    var currentPaint = initialPaint;
    for (var x = 0; x < result.noOfModulesX; x += receiverLayoutX) {
      currentReceiver++;
      for (var dx = 0; dx < receiverLayoutX && x + dx < result.noOfModulesX; dx++)
        for (var dy = 0; dy < result.cabinetsY[currentCab] && y + dy < result.noOfModulesY; dy++) {
          layout.rows[y + dy].modules[x + dx].receiver = {
            number: currentReceiver,
            paint: currentPaint
          };
        }
      currentPaint = !currentPaint;
    }
    initialPaint = !initialPaint;
  }

  result.noOfReceivers = Math.ceil(result.noOfModulesX / receiverLayoutX) * result.cabinetsY.length;

  result.layout = layout;
  return result;

}
